package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PersonneRepository;
import com.example.demo.models.Personne;

@RestController
public class PersonneRestController {
	@Autowired
	private PersonneRepository personneRepository;

	@RequestMapping(value = "/personnes", method = RequestMethod.GET)
	public List<Personne> getPersonnes() {
		return personneRepository.findAll();
	}

	@RequestMapping(value = "/personnes/{id}", method = RequestMethod.GET)
	public Personne getPersonneById(@PathVariable Long id) {
		return personneRepository.findById(id).get();
	}

	@RequestMapping(value = "/personnes", method = RequestMethod.POST)
	public Personne save(@RequestBody Personne p) {
		return personneRepository.save(p);
	}

	@RequestMapping(value = "/personnes/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id) {
		personneRepository.deleteById(id);
		return true;
	}

	@RequestMapping(value = "/personnes/{id}", method = RequestMethod.PUT)
	public Personne save(@PathVariable Long id, @RequestBody Personne p) {
		p.setNum(id);
		return personneRepository.save(p);
	}
}